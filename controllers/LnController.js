const Lightning = require('./../libs/_rpc_pb_service').Lightning;
const PaymentHash = require('./../libs/rpc_pb').PaymentHash;
const grpc = require('grpc-web-client').grpc;
const Code = require('grpc-web-client').Code;
const configNode = require('./../config/nodeConfig');

class LnController {
    static checkInvoice(rHash, callback){
        let paymenthash = new PaymentHash();
        paymenthash.setRHash(rHash);
        return grpc.unary(Lightning.LookupInvoice, {
            request: paymenthash,
            host: configNode.host,
            onEnd: callback
        });
    }
}

module.exports = LnController;
