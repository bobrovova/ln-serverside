const router = (require('express').Router)();
const grpc = require('grpc-web-client').grpc;
const Code = require('grpc-web-client').Code;
const Lightning = require('./../../libs/_rpc_pb_service').Lightning;
const Invoice = require('./../../libs/rpc_pb').Invoice;
const PaymentHash = require('./../../libs/rpc_pb').PaymentHash;
const log = require('./../../logger');
const configNode = require('./../../config/nodeConfig');
const toHex = require('./../../utils/toHex');
const LnController = require('./../LnController');

//Example of paid page
router.get('/', function (req, res) {
    //Check if the extension is installed
    if(typeof req.headers['x-payment-proof'] === 'undefined'){
        res.send({
            status: 200,
            response: {
                canYouSeeContent: 'Please, install extension'
            }
        });
        return;
    }

    const rHash = req.headers['x-payment-proof'];
    if(rHash != 'null') {
        let callback = (function(result){
            let status = result.status;
            let message = result.message;
            if (status === Code.OK && message) {
                //Обработать, если нет
                if(message.toObject().settled == true){
                    //Redirect to unique link
                    res.send({
                        status: 200,
                        response: {
                            canYouSeeContent: true
                        }
                    })
                }
            } else {
                log.error(result);
                res.send({
                    status: 200,
                    response: {
                        canYouSeeContent: false
                    }
                });
            }
        }).bind(this);

        LnController.checkInvoice(rHash, callback);
    } else {
        res.send({
            status: 200,
            response: {
                canYouSeeContent: 'Please, buy this content'
            }
        });
    }
});

/**
*   API Method for add invoice
*/
router.get('/addInvoice', function (req, res) {
    let invoiceRequest = new Invoice();

    //Somebody should set amount on server-side or check user amount
    invoiceRequest.setValue(102);
    grpc.unary(Lightning.AddInvoice, {
        request: invoiceRequest,
        host: configNode.host,
        onEnd: function(result) {
            let status = result.status;
            let message = result.message;
            if (status === Code.OK && message) {
                res.send({
                    status: 200,
                    response: message.toObject()
                });
            } else {
                log.error(result);
                res.send({
                    status: 500,
                    response: result
                });
            }
        }
    });
});

/**
*   API Method for check invoice by rHash
*/
router.get('/checkInvoice/:rHash', function (req, res) {
    const rHash = req.params.rHash;
    let paymenthash = new PaymentHash();
    paymenthash.setRHash(rHash);

    grpc.unary(Lightning.LookupInvoice, {
        request: paymenthash,
        host: configNode.host,
        onEnd: function(result) {
            let status = result.status;
            let message = result.message;
            if (status === Code.OK && message) {
                //Обработать, если нет
                if(message.toObject().response.settled == true){
                    //Redirect to unique link
                    res.send({
                        status: 200,
                        response: message.toObject()
                    })
                }
            } else {
                log.error(result);
                res.send({
                    status: 500,
                    response: result
                });
            }
        }
    });
});

module.exports = router;
