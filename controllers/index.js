let app = new (require('express').Router)();

app.use('/ln', require('./api/lnControllerApi'));

module.exports = app;
